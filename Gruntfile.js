/* =========================================================
 * Gruntfile.js
 * =========================================================
 * Copyright 2018 Tim Töws
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */

function removePath(dest, src, vendor = false) {
	var path = require('path');
	var srcSplit = src.split(path.sep);
		if(!vendor) srcSplit.splice(1, 1);
		if(vendor) srcSplit.splice(srcSplit.length - 2, 1);
	var newPath = path.join(dest, srcSplit.join(path.sep));

	return newPath;
}

module.exports = function (grunt) {
	'use strict';

	grunt.util.linefeed = '\n';

	grunt.initConfig({
		sass: {
			dist : {
				options: {
					sourceMap: true
				},
				files: [{
					expand: true,
					cwd: 'tabdrop',
					src: ['**/*.scss'],
					dest: 'dist/',
					ext: '.css'
				}]
			}
		},
		postcss: {
			dist : {
				options: {
					map: true,
					processors: [
						require('autoprefixer')({ browsers: 'last 2 version, IE 9' }),
						require('cssnano')({ discardComments: { removeAll: true } })
					]
				},
				files: [{
					expand: true,
					cwd: 'dist/',
					src: ['**/*.css'],
					dest: 'dist/',
					ext: '.min.css'
				}]
			}
		},
		copy: {
			js : {
				files: [
					{ expand: true, src: ['*/javascripts/**/*.js', '!node_modules/**', '!dist/**'], dest: 'dist/javascripts', rename : function(dest, src) { return removePath(dest, src); } },
					{ expand: true, src: ['*/vendors/**/*.js', '!node_modules/**', '!dist/**'], dest: 'dist/javascripts', rename : function(dest, src) { return removePath(dest, src, true); } }
				]
			},
			others : {
				files: [
					{ expand: true, src: ['*/vendors/**/others/**', '!node_modules/**', '!dist/**'], dest: 'dist/others', rename : function(dest, src) { return removePath(dest, src, true); } }
				]
			}
		},

		clean: {
			dist: ['dist']
		},

		watch: {
			sass : {
				files : ['**/*.scss', '!dist/**'],
				tasks: ['sass', 'postcss']
			},
			js : {
				files : ['**/*.js', '!dist/**', '!**/Gruntfile.js'],
				tasks: ['copy:js']
			}
		}
	});

	grunt.registerTask('compile', ['clean', 'sass', 'postcss', 'copy']);
	grunt.registerTask('watch', ['watch']);

	require('load-grunt-tasks')(grunt, { scope: 'devDependencies' });
};
